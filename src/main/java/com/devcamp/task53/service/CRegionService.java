package com.devcamp.task53.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.task53.model.CRegion;

@Service
public class CRegionService {
    private static List<CRegion> regionVietNam = new ArrayList<CRegion>();
    private static List<CRegion> regionUs = new ArrayList<CRegion>();
    private static List<CRegion> regionRusia = new ArrayList<CRegion>();

    static {
        CRegion HaiDuong = new CRegion("Hải Dương", 34);
        CRegion HaNoi = new CRegion("Hà Nội", 29);
        CRegion TpHCM = new CRegion("Tp Hồ Chí Minh",59);
        CRegion newyork = new CRegion("New York", 198);
        CRegion Florida = new CRegion("Florida", 334);
        CRegion texas = new CRegion("Texas", 341);
        CRegion moscow = new CRegion("Moscow", 361);
        CRegion kuluga = new CRegion("Kuluga", 12);
        CRegion sainpeter = new CRegion("Saipeter", 98);

        regionVietNam.add(HaiDuong);
        regionVietNam.add(HaNoi);
        regionVietNam.add(TpHCM);
        regionUs.add(newyork);
        regionUs.add(Florida);
        regionUs.add(texas);
        regionRusia.add(moscow);
        regionRusia.add(kuluga);
        regionRusia.add(sainpeter);
    }

    public static List<CRegion> getRegionVietNam() {
        return regionVietNam;   
    }

    public static void setRegionVietNam(List<CRegion> regionVietNam) {
        CRegionService.regionVietNam = regionVietNam;
    }

    public static List<CRegion> getRegionUs() {
        return regionUs;
    }

    public static void setRegionUs(List<CRegion> regionUs) {
        CRegionService.regionUs = regionUs;
    }

    public static List<CRegion> getRegionRusia() {
        return regionRusia;
    }

    public static void setRegionRusia(List<CRegion> regionRusia) {
        CRegionService.regionRusia = regionRusia;
    }

    
}
