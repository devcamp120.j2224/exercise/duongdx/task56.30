package com.devcamp.task53.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task53.model.CCountry;

@Service
public class CCountryService {
    private static List<CCountry> countryList = new ArrayList<CCountry>();

    @Autowired
    static CRegionService Regions;

    static {
        CCountry vietnam = new CCountry("Viet Nam" , 2021 , null); 
        CCountry us = new CCountry("Hoa Ky" , 2121 , null); 
        CCountry rusia = new CCountry("nga" , 2321 , null); 

        countryList.add(vietnam);
        countryList.add(us);
        countryList.add(rusia);

        for(int i = 0 ; i < countryList.size() ; i++){
            if(countryList.get(i).getCountryName() == "Viet Nam"){
                countryList.get(i).setRegions(CRegionService.getRegionVietNam());
            }else if(countryList.get(i).getCountryName() == "Hoa Ky"){
                countryList.get(i).setRegions(CRegionService.getRegionUs());
            }
            else if(countryList.get(i).getCountryName() == "nga"){
                countryList.get(i).setRegions(CRegionService.getRegionRusia());
            }
        }
    }
    public CCountryService(){
        super();
    }
    public static List<CCountry> getCountryList() {
        return countryList;
    }
    public static void setCountryList(List<CCountry> countryList) {
        CCountryService.countryList = countryList;
    }
  

}
