package com.devcamp.task53;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task53Application {

	public static void main(String[] args) {
		SpringApplication.run(Task53Application.class, args);
	}

}
