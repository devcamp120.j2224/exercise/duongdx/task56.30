package com.devcamp.task53.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task53.model.CCountry;
import com.devcamp.task53.model.CRegion;
import com.devcamp.task53.service.CCountryService;

@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
@RestController
public class CCountryController {
    @Autowired
    static CCountryService countries;
    
    @GetMapping("/countries")
    public List<CCountry> getCountryList() throws Exception {
        List<CCountry> allCountries = CCountryService.getCountryList();
        return allCountries;
    }
    @GetMapping("/country")
    public Map<String, Object> getRegionOfCountry(@RequestParam(required = true, name = "countrycode") int id) throws Exception {
        Map<String, Object> returnobj = new HashMap<String, Object>();
        List<CRegion> regionsList = null;

        int i = 0 ;
        boolean isFounder = false;
        while (!isFounder == true && i < CCountryService.getCountryList().size()){
            if(CCountryService.getCountryList().get(i).getCountryCode() ==  id){
                regionsList = CCountryService.getCountryList().get(i).getRegions();
                isFounder = true;
                returnobj.put("region", regionsList);
                returnobj.put("status", new String("ok"));

            }else{
                i++;
                returnobj.put("region", null);
                returnobj.put("status", new String("Not Found"));
            }

        }
        return returnobj ;
    }
}